package com.example.admin.learningenglish.views;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.admin.learningenglish.R;

public class TranslationActivity extends AppCompatActivity {
    TextView txtTitle;
    ImageView imgBack, imgChoose;
    private int choose = 0;

    private WebView wvNghia;
    private TextView txtTu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_translation);
        addActionBar();
        addControl();
        addEvent();
    }

    private void addActionBar() {
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.actionbar_custom_2);
        txtTitle = (TextView) findViewById(R.id.txtTitle);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        imgChoose = (ImageView) findViewById(R.id.imgChoose);

        txtTitle.setText("Translation");
        imgChoose.setImageResource(R.drawable.starfalse);
    }

    private void addEvent() {
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        imgChoose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (choose == 0) {
                    imgChoose.setImageResource(R.drawable.startrue);
                    choose = 1;
                }
                else {
                    imgChoose.setImageResource(R.drawable.starfalse);
                    choose = 0;
                }
            }
        });
    }

    private void addControl() {
        wvNghia = (WebView) findViewById(R.id.wvNghia);
        txtTu = (TextView) findViewById(R.id.txtTu);

        //test dữ liệu
        String data = "<span class=\"title\">Chuyên ngành kinh tế</span><br /><ul><li>giấy phép \"A\"</li><li>môn bài \"A\"</li></ul>";
//        wvTraTu.loadData(data, "text/html", "UTF-8");
        wvNghia.loadDataWithBaseURL(null, data, "text/html", "UTF-8", null);

        txtTu.setText("license");
    }
}
