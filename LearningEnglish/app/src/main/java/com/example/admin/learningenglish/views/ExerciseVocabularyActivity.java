package com.example.admin.learningenglish.views;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.admin.learningenglish.R;

public class ExerciseVocabularyActivity extends AppCompatActivity {
    ImageView imgBack;
    TextView txtSoCau, txtTongSoCau, txtDiem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise_vocabulary);
        addActionBar();
        addControl();
        addEvent();
    }

    private void addActionBar() {

    }

    private void addEvent() {
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void addControl() {
    }
}
