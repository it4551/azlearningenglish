package com.example.admin.learningenglish.databases.vocabulary;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

import com.example.admin.learningenglish.R;
import com.example.admin.learningenglish.databases.vocabulary.LessionContract.LessionEntry;
import com.example.admin.learningenglish.databases.vocabulary.WordContract.WordEntry;
import com.example.admin.learningenglish.models.vocabulary.Lession;
import com.example.admin.learningenglish.models.vocabulary.VocabWord;

/**
 * Created by zznam on 02-Nov-17.
 */

public class MyDbHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "VocabWords.db";
    public static final int DATABASE_VERSION = 1;

    public MyDbHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        String SQL_CREATE_ENTRIES_WORDS =
                "CREATE TABLE " +WordEntry.TABLE_NAME + " (" +
                        WordEntry.COLUMN_WORD_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "+
                        WordEntry.COLUMN_DEFAULT_TRANSLATION + " TEXT NOT NULL, "+
                        WordEntry.COLUMN_VIETNAMESE_TRANSLATION + " TEXT, "+
                        WordEntry.COLUMN_IPA_PRONUNCIATION + " TEXT ," +
                        WordEntry.COLUMN_EXAMPLE + " TEXT, " +
                        WordEntry.COLUMN_LESSION_ID + " INT, " +
                        WordEntry.COLUMN_WORD_CLASS + " TEXT, " +
                        WordEntry.COLUMN_AUDIO_RES_ID + " INTEGER NOT NULL DEFAULT -1, " +
                        WordEntry.COLUMN_IS_IN_REMIND_LIST + " INTEGER NOT NULL DEFAULT 0, " +
                        WordEntry.COLUMN_REMIND_TIME + " TEXT, " +
                        WordEntry.COLUMN_IMAGE_RES_ID + " INTEGER NOT NULL DEFAULT -1);" ;
        String SQL_CREATE_ENTRIES_LESSIONS =
                "CREATE TABLE " + LessionEntry.TABLE_NAME + " (" +
                        LessionEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"+
                        LessionEntry.COLUMN_LESSION_NAME + " TEXT NOT NULL,"+
                        LessionEntry.COLUMN_LESSION_LEVEL + " INT, " +
                        LessionEntry.COLUMN_LESSION_SCORE + " INTEGER NOT NULL DEFAULT 0, " +
                        LessionEntry.COLUMN_NUMBER_OF_WORDS + " INT, " +
                        LessionEntry.COLUMN_LAST_TIME_ACCESSED + " TEXT);" ;


        db.execSQL(SQL_CREATE_ENTRIES_WORDS);
        db.execSQL(SQL_CREATE_ENTRIES_LESSIONS);
        
        initValues(db);
        
    }

    private void initValues(SQLiteDatabase db) {
        ArrayList<VocabWord> words = new ArrayList<>();
        words.add(new VocabWord("number","one","một","wən","there's only room for one person",1, R.drawable.number_one,R.raw.number_one, WordEntry.NO));
        words.add(new VocabWord("number","two","hai","to͞o","two years ago",1, R.drawable.number_two,R.raw.number_two, WordEntry.NO));
        words.add(new VocabWord("number","three","ba","THrē","her three children",1, R.drawable.number_three,R.raw.number_three, WordEntry.NO));
        words.add(new VocabWord("number","four","bốn","fôr","Francesca's got four brothers",1, R.drawable.number_four,R.raw.number_four, WordEntry.NO));
        words.add(new VocabWord("number","five","năm","fīv","a circlet of five petals",1, R.drawable.number_five,R.raw.number_five, WordEntry.NO));
        words.add(new VocabWord("number","six","sáu","siks","she's lived here six months",1, R.drawable.number_six,R.raw.number_six, WordEntry.NO));
        words.add(new VocabWord("number","seven","bảy","ˈsevən","two sevens are fourteen",1, R.drawable.number_seven,R.raw.number_seven, WordEntry.NO));
        words.add(new VocabWord("number","eight","tám","āt","a committee of eight members",1, R.drawable.number_eight,R.raw.number_eight, WordEntry.NO));
        words.add(new VocabWord("number","nine","chín","nine","all nine justices agreed that the law could not stand",1, R.drawable.number_nine,R.raw.number_nine, WordEntry.NO));
        words.add(new VocabWord("number","ten","mười","ten","the last ten years",1, R.drawable.number_ten,R.raw.number_ten, WordEntry.NO));
        words.add(new VocabWord("adj","red","đỏ","red","her red lips",2, R.drawable.color_red,R.raw.red, WordEntry.NO));
        words.add(new VocabWord("adj","green","xanh lá cây","ɡrēn","",2, R.drawable.color_green,R.raw.green, WordEntry.NO));
        words.add(new VocabWord("adj","gray","xám","ɡrā","gray flannel trousers",2, R.drawable.color_gray,R.raw.gray, WordEntry.NO));
        words.add(new VocabWord("adj","black","đen","blak","a black horse",2, R.drawable.color_black,R.raw.black, WordEntry.NO));
        words.add(new VocabWord("adj","white","trắng","(h)wīt","a sheet of white paper",2, R.drawable.color_white,R.raw.white, WordEntry.NO));
        words.add(new VocabWord("adj","dusty yellow","vàng nhạt","ˈdəstē","Nununu Hooded Ninja Shirt in Dusty Yellow",2, R.drawable.color_dusty_yellow,R.raw.yellow, WordEntry.NO));
        words.add(new VocabWord("adj","mustard yellow","vàng đậm","ˈməstərd","A 2 seater sofa, in Mustard Yellow",2, R.drawable.color_mustard_yellow,R.raw.yellow, WordEntry.NO));
        words.add(new VocabWord("adj","purple","tím","ˈpərpəl","a faded purple T-shirt",2, R.drawable.color_purple,R.raw.purple, WordEntry.NO));
        words.add(new VocabWord("n","father","bố","ˈfäT͟Hər","My father gave me the greatest gift anyone could give another person, he believed in me.",3,R.drawable.family_father,R.raw.father, WordEntry.NO));
        words.add(new VocabWord("n","mother","mẹ","ˈməT͟Hər","she returned to Bristol to nurse her aging mother",3,R.drawable.family_mother,R.raw.mother, WordEntry.NO));
        words.add(new VocabWord("n","son","con trai","sən","the sons of Adam",3,R.drawable.family_son,R.raw.son, WordEntry.NO));
        words.add(new VocabWord("n","daughter","con gái","ˈdôdər","their two daughters are both in college",3,R.drawable.family_daughter,R.raw.daughter, WordEntry.NO));
        words.add(new VocabWord("n","older brother","anh trai ","ˈoʊl dər ˈbrʌð ər ","Older brother and younger sister playing video games",3,R.drawable.family_older_brother,R.raw.brother, WordEntry.NO));
        words.add(new VocabWord("n","younger brother","em trai","ˈyʌŋ gər ˈbrʌð ər","Sister lifting up younger brother",3,R.drawable.family_younger_brother,R.raw.brother, WordEntry.NO));
        words.add(new VocabWord("n","older sister","chị gái","ˈoʊl dər ˈsɪs tər","Older sister putting younger brother in his place",3,R.drawable.family_older_sister,R.raw.sister, WordEntry.NO));
        words.add(new VocabWord("n","younger sister","em gái","ˈyʌŋ gər ˈsɪs tər","How to Deal When Your Younger Sister Is One Thousand Times Cooler Than You",3,R.drawable.family_younger_sister,R.raw.sister, WordEntry.NO));
        words.add(new VocabWord("n","grandmother","bà","ˈgrænˌfɑ ðər","the mother of one's father or mother.",3,R.drawable.family_grandmother,R.raw.grandmother, WordEntry.NO));
        words.add(new VocabWord("n","grandfather","ông","ˈgrænˌmʌð ər","his grandfather lives here",3,R.drawable.family_grandfather,R.raw.grandfather, WordEntry.NO));
        words.add(new VocabWord("test","zznam","namzz","ˈgrænˌmʌð ər","Lalawn",3,R.drawable.family_grandfather,R.raw.grandfather, WordEntry.YES));



        for (VocabWord word: words){
            ContentValues values = new ContentValues();
            values.put(WordEntry.COLUMN_WORD_CLASS, word.getWordClass());
            values.put(WordEntry.COLUMN_DEFAULT_TRANSLATION, word.getDefaultTranslation());
            values.put(WordEntry.COLUMN_VIETNAMESE_TRANSLATION, word.getVietnameseTranslation());
            values.put(WordEntry.COLUMN_IPA_PRONUNCIATION, word.getIpaPronounciation());
            values.put(WordEntry.COLUMN_EXAMPLE, word.getExample());
            values.put(WordEntry.COLUMN_LESSION_ID, word.getLessionID());
            values.put(WordEntry.COLUMN_IMAGE_RES_ID, word.getImageResourceId());
            values.put(WordEntry.COLUMN_AUDIO_RES_ID, word.getAudioResourceId());
            values.put(WordEntry.COLUMN_IS_IN_REMIND_LIST, word.getIsInRemindList());
//            values.put(WordEntry.COLUMN_REMIND_TIME, word.getRemindTime());
            db.insert(WordEntry.TABLE_NAME, null,  values);
        }
        ArrayList<Lession> lessions = new ArrayList<>();
        lessions.add(new Lession(1,"Number", 0));
        lessions.add(new Lession(2,"Colors", 0));
        lessions.add(new Lession(3,"Family", 0));
        lessions.add(new Lession(4,"Simple Phrases", 0));
        lessions.add(new Lession(5,"Greetings", 0));
        lessions.add(new Lession(6,"At School", 0));
        lessions.add(new Lession(7,"At Home", 0));
        lessions.add(new Lession(8,"Big or Small", 0));
        lessions.add(new Lession(9,"Things I Do", 0));
        lessions.add(new Lession(10,"Places", 0));
        lessions.add(new Lession(11,"Your House", 0));
        lessions.add(new Lession(12,"Out And Out", 0));
        lessions.add(new Lession(13,"The Body", 1));
        lessions.add(new Lession(14,"Stayting Healthy", 1));
        lessions.add(new Lession(15,"What do You Eat", 1));
        lessions.add(new Lession(16,"Sports and Pastimes", 1));
        lessions.add(new Lession(17,"Activities and The Seasons", 1));
        lessions.add(new Lession(18,"Making Plans", 2));
        lessions.add(new Lession(19,"Countries", 2));
        lessions.add(new Lession(20,"My Words", 2));


        for (Lession lession: lessions){
            ContentValues values = new ContentValues();

            values.put(LessionEntry.COLUMN_LESSION_NAME, lession.getName());
            values.put(LessionEntry.COLUMN_LESSION_LEVEL, lession.getLevel());
            values.put(LessionEntry.COLUMN_LESSION_SCORE, lession.getScore());
//            //todo
//            values.put(LessionEntry.COLUMN_LESSION_SCORE, 0);

            values.put(LessionEntry.COLUMN_NUMBER_OF_WORDS, lession.getNumberOfWords());
            values.put(LessionEntry.COLUMN_LAST_TIME_ACCESSED, lession.getLastTimeAccess());



            db.insert(LessionEntry.TABLE_NAME, null,  values);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String SQL_DELETE_ENTRIES_WORDS =
                "DROP TABLE IF EXISTS " + WordEntry.TABLE_NAME;
        String SQL_DELETE_ENTRIES_LESSIONS =
                "DROP TABLE IF EXISTS " + WordEntry.TABLE_NAME;

        db.execSQL(SQL_DELETE_ENTRIES_WORDS);
        db.execSQL(SQL_DELETE_ENTRIES_LESSIONS);
        onCreate(db);

    }
}
