package com.example.admin.learningenglish.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.admin.learningenglish.views.ContentVocaularyActivity;
import com.example.admin.learningenglish.views.MainActivity;
import com.example.admin.learningenglish.R;

/**
 * Created by admin on 10/7/2017.
 */

public class VocabularyFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_vocabulary,container,false);

        addControl(view);
        addEvent();

        return view;
    }

    private void addEvent() {
    }

    private void addControl(View view) {
        //test dữ liệu
        Button btn = (Button) view.findViewById(R.id.btnHocTiep);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ContentVocaularyActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        MainActivity.txtTitle.setText("Vocabulary");
    }
}
