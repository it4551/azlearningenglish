package com.example.admin.learningenglish.views;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.admin.learningenglish.R;

public class ContentVocaularyActivity extends AppCompatActivity {
    ImageView imgBack;
    TextView txtTitle;

    Button btnStart;
    WebView wvNoiDung;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content_vocaulary);
        addActionBar();
        addControl();
        addEvent();
    }

    private void addActionBar() {
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.actionbar_custom_3);
        txtTitle = (TextView) findViewById(R.id.txtTitle);
        imgBack = (ImageView) findViewById(R.id.imgBack);

        txtTitle.setText("Bài 1");


    }

    private void addEvent() {
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        //test dữ liệu
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ContentVocaularyActivity.this,ExerciseVocabularyActivity.class);
                startActivity(intent);
            }
        });
    }

    private void addControl() {
        wvNoiDung = (WebView) findViewById(R.id.wvNoiDung);
        btnStart = (Button) findViewById(R.id.btnStart);

        //test dữ liệu
        String data = "<span class=\"title\">Chuyên ngành kinh tế</span><br /><ul><li>giấy phép \"A\"</li><li>môn bài \"A\"</li></ul>";
        wvNoiDung.loadDataWithBaseURL(null, data, "text/html", "UTF-8", null);
    }
}
