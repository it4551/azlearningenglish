package com.example.admin.learningenglish.fragments.vocabulary_fragments;


import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Date;

import com.example.admin.learningenglish.R;
import com.example.admin.learningenglish.adapters.vocabulary.LessionCursorAdapter;
import com.example.admin.learningenglish.databases.vocabulary.LessionContract.LessionEntry;
import com.example.admin.learningenglish.databases.vocabulary.WordContract;
import com.example.admin.learningenglish.models.vocabulary.Lession;
import com.example.admin.learningenglish.views.vocabulary.WordsActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class FirstFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>{

    private ArrayList<Lession> lessions = new ArrayList<>();

    private ListView listViewLessions;

    /** Identifier for the pet data loader */
    public static final int LESSION_LOADER = 0;

    /** Adapter for the ListView */
    LessionCursorAdapter mCursorAdapter;

    public FirstFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_first, container, false);;




        listViewLessions = (ListView) view.findViewById(R.id.listViewLessions);

        mCursorAdapter = new LessionCursorAdapter(getContext(), null);
//        lessionAdapter = new LessionAdapter(this.getContext(), R.layout.list_item_lession, lessions);


        listViewLessions.setAdapter(mCursorAdapter);



        listViewLessions.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                Lession lession = (Lession) listViewLessions.getItemAtPosition(position);
//                Log.v("ZZ","pos:" + position+"" + "   id  " + id);
//                Cursor cursor =((CursorAdapter)listViewLessions.getAdapter()).getCursor();
//                cursor.moveToPosition(position);
//                Cursor cursor = (Cursor) parent.getAdapter().getItem(position);
//                String lessionId = String.valueOf(cursor.getColumnIndexOrThrow(LessionEntry._ID));


                updateLession((int) id);
                Log.e("FirstFragment", String.valueOf(Uri.withAppendedPath(WordContract.WordEntry.WORDS_OF_LESSION_CONTENT_URI, String.valueOf(id))));
                Intent intent = new Intent(getContext(), WordsActivity.class);
                intent.setData(Uri.withAppendedPath(WordContract.WordEntry.WORDS_OF_LESSION_CONTENT_URI, String.valueOf(id)));
                startActivity(intent);
            }
        });

        // Kick off the loader
        getLoaderManager().initLoader(LESSION_LOADER, null,this);

        return view;
    }

    private void updateLession(int id) {
        Date date = new Date();
        Log.e(id + "", String.valueOf(date.getTime()));
        ContentValues values = new ContentValues();
        values.put(LessionEntry.COLUMN_LAST_TIME_ACCESSED, String.valueOf(date.getTime()));
        Uri currentLessionUri = Uri.withAppendedPath(LessionEntry.LESSIONS_CONTENT_URI, String.valueOf(id));
        int updateRows = getContext().getContentResolver().update(currentLessionUri,values, null, null);
    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Uri uri = LessionEntry.LESSIONS_CONTENT_URI;
        String projecton[] = {
                LessionEntry._ID,
                LessionEntry.COLUMN_LESSION_NAME,
                LessionEntry.COLUMN_LESSION_LEVEL,
                LessionEntry.COLUMN_NUMBER_OF_WORDS,
                LessionEntry.COLUMN_LAST_TIME_ACCESSED

        };
        String selection = null;
        String[] selectionArgs = null;
        if(args != null){
            selection = LessionEntry.COLUMN_LESSION_NAME + " like ?";
            selectionArgs = new String[] {"%" + args.getString("SEARCH_QUERY") + "%"};
        }


        return new CursorLoader(getContext(), uri, projecton, selection, selectionArgs, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

        mCursorAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mCursorAdapter.swapCursor(null);
    }
}
