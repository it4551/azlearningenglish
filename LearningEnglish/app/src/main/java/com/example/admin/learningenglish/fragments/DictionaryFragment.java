package com.example.admin.learningenglish.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import com.example.admin.learningenglish.adapters.DictionaryAdapter;
import com.example.admin.learningenglish.views.MainActivity;
import com.example.admin.learningenglish.R;
import com.example.admin.learningenglish.views.RecentActivity;
import com.example.admin.learningenglish.views.TranslationActivity;
import com.example.admin.learningenglish.views.WordMarkActivity;

/**
 * Created by admin on 10/7/2017.
 */

public class DictionaryFragment extends Fragment {
    private ListView lvTuDien;
    private ImageView imgTimKiem;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dictionary,container,false);

        addControl(view);
        addEvent();
        khoiTao();

        return view;
    }

    private void khoiTao() {
        String[] ten = {
                "Từ vừa tra",
                "Đánh dấu",
                "Tra từ qua ảnh"
        };

        int[] hinh = {
                R.drawable.word_recent,
                R.drawable.word_mark,
                R.drawable.translation_image
        };

        DictionaryAdapter dictionaryAdapter = new DictionaryAdapter(getActivity(),ten,hinh);
        lvTuDien.setAdapter(dictionaryAdapter);
    }

    private void addEvent() {
        lvTuDien.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent;
                switch (i) {
                    case 0:
                        intent = new Intent(getActivity(),RecentActivity.class);
                        startActivity(intent);
                        break;
                    case 1:
                        intent = new Intent(getActivity(),WordMarkActivity.class);
                        startActivity(intent);
                        break;
                    case 2:
                        //tra từ qua hình ảnh
                        break;
                }
            }
        });

        imgTimKiem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(),TranslationActivity.class);
                startActivity(intent);
            }
        });
    }

    private void addControl(View view) {
        lvTuDien = (ListView) view.findViewById(R.id.lvTuDien);
        imgTimKiem = (ImageView) view.findViewById(R.id.imgTimKiem);
    }

    @Override
    public void onResume() {
        super.onResume();
        MainActivity.txtTitle.setText("Dictionary");
    }
}
