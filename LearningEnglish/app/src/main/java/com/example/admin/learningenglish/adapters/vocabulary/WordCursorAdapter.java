package com.example.admin.learningenglish.adapters.vocabulary;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.speech.RecognizerIntent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.widget.CursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import com.example.admin.learningenglish.R;
import com.example.admin.learningenglish.databases.vocabulary.WordContract;
import com.example.admin.learningenglish.databases.vocabulary.WordContract.WordEntry;
import com.example.admin.learningenglish.services.ReminderService;
import com.example.admin.learningenglish.views.vocabulary.EditorActivity;

/**
 * Created by zznam on 11/4/2017.
 */

public class WordCursorAdapter extends CursorAdapter {
    private static final int REQ_CODE_SPEECH_INPUT = 343;
    Activity context;
    public WordCursorAdapter(Context context, Cursor c) {
        super(context, c, 0);
        this.context = (Activity) context;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.list_item_word, parent, false);
    }

    @Override
    public void bindView(final View view, final Context context, Cursor cursor) {
        ImageView imgView = (ImageView) view.findViewById(R.id.imgView);
        final TextView txtEnglish = (TextView) view.findViewById(R.id.txtEnglish);
        final TextView txtDefault = (TextView) view.findViewById(R.id.txtDefault);
        final ImageView btnReminder = (ImageView) view.findViewById(R.id.btnReminder);
        ImageView btnWordOptions = (ImageView) view.findViewById(R.id.btnWordOptions);
        final int _id = cursor.getInt(cursor.getColumnIndexOrThrow(WordContract.WordEntry._ID));
        final String wordClass = cursor.getString(cursor.getColumnIndexOrThrow(WordContract.WordEntry.COLUMN_WORD_CLASS));
        final String englishTranslation = cursor.getString(cursor.getColumnIndexOrThrow(WordContract.WordEntry.COLUMN_DEFAULT_TRANSLATION));
        String vietnameseTranslation = cursor.getString(cursor.getColumnIndexOrThrow(WordContract.WordEntry.COLUMN_VIETNAMESE_TRANSLATION));
        String ipaPronunciation = cursor.getString(cursor.getColumnIndexOrThrow(WordContract.WordEntry.COLUMN_IPA_PRONUNCIATION));
        final String example = cursor.getString(cursor.getColumnIndexOrThrow(WordContract.WordEntry.COLUMN_EXAMPLE));
//        int lessionId = cursor.getInt(cursor.getColumnIndexOrThrow(WordContract.WordEntry.COLUMN_LESSION_ID));
        int imageResourceId = cursor.getInt(cursor.getColumnIndexOrThrow(WordContract.WordEntry.COLUMN_IMAGE_RES_ID));
        int audioResourceId = cursor.getInt(cursor.getColumnIndexOrThrow(WordContract.WordEntry.COLUMN_AUDIO_RES_ID));
        final int isInRemindList = cursor.getInt(cursor.getColumnIndexOrThrow(WordContract.WordEntry.COLUMN_IS_IN_REMIND_LIST));
        if (englishTranslation.length() > 10){
            txtEnglish.setText(englishTranslation + " - (" + wordClass + ")" );
        }else {
            txtEnglish.setText(englishTranslation + " - (" + wordClass + ") - /" + ipaPronunciation + "/");
        }

        txtDefault.setText(vietnameseTranslation);
        if (imageResourceId!=-1) {
            imgView.setImageResource(imageResourceId);
        }
        if (isInRemindList == WordContract.WordEntry.NO){
            btnReminder.setVisibility(View.INVISIBLE);
        }
        if (isInRemindList == WordContract.WordEntry.YES){
            btnReminder.setVisibility(View.VISIBLE);
        }
//
//        view.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//            }
//        });
        btnWordOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popupMenu = new PopupMenu(context, v);
                popupMenu.getMenuInflater().inflate(R.menu.menu_popup, popupMenu.getMenu());;
                popupMenu.show();
                MenuItem item = popupMenu.getMenu().getItem(0);

                if (isInRemindList == WordContract.WordEntry.NO){
                    item.setTitle(R.string.remind);
                }
                if (isInRemindList == WordContract.WordEntry.YES){
                    item.setTitle(R.string.unremind);
                }
                Log.e("ZZ1",item.getTitle().toString());
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()){
                            case R.id.remind:
                                if (isInRemindList == WordContract.WordEntry.NO){
                                    // todo updateDb();
                                    item.setTitle(R.string.unremind);
                                    btnReminder.setVisibility(View.VISIBLE);

//                                    testNotify(_id, englishTranslation, wordClass, example);
                                }
                                if (isInRemindList == WordContract.WordEntry.YES){
                                    // todo updateDb();
                                    btnReminder.setVisibility(View.INVISIBLE);
                                    item.setTitle(R.string.remind);
                                }
                                updateWord(_id, isInRemindList);
                                // todo
                                Log.e("ZZ2",item.getTitle().toString());
                                break;
                            case R.id.show_example:
                                txtDefault.setText(example);
                                break;
                            case R.id.pronunciation_test:
                                promtSpeechInput(_id, englishTranslation);
                                break;
                            case R.id.action_edit_word:
                                Intent intent = new Intent(context, EditorActivity.class);
                                intent.setData(Uri.withAppendedPath(WordContract.WordEntry.WORDS_CONTENT_URI, String.valueOf(_id)));
                                context.startActivity(intent);
                            default: break;
                        }
                        return true;
                    }
                });
            }
        });

    }

    //todo fix alarm notify after somtimes config
    private void startAlarm(int id) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);
//        Calendar
        Uri currentWordUri = Uri.withAppendedPath(WordContract.WordEntry.WORDS_CONTENT_URI, String.valueOf(id));

        long remindTime = Calendar.getInstance().getTimeInMillis();
        String projection[] = {
                WordEntry.COLUMN_REMIND_TIME
        };
        Cursor cursor = context.getContentResolver().query(
                currentWordUri,
                projection,
                null,
                null,
                null);
        if (cursor.moveToFirst()) {
            remindTime = Long.parseLong(cursor.getString(cursor.getColumnIndexOrThrow(WordEntry.COLUMN_REMIND_TIME)));
        }
        remindTime += 10000;
        Intent intent = new Intent(context, ReminderService.class);
        intent.setData(currentWordUri);
        PendingIntent pendingIntent = PendingIntent.getService(context, 0, intent, 0);
        alarmManager.set(AlarmManager.RTC, remindTime, pendingIntent);


    }

    private void testNotify(int _id, String englishTranslation, String wordClass, String example) {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.ic_notifications_paused_black_24dp)
                        .setContentTitle(englishTranslation + " - (" + wordClass + ")")
                        .setContentText(example)
                        .setTicker(englishTranslation)
                ;
        Intent intent = new Intent(context, EditorActivity.class);
        intent.setData(Uri.withAppendedPath(WordContract.WordEntry.WORDS_CONTENT_URI, String.valueOf(_id)));
        PendingIntent resultPedingIntent =
                PendingIntent.getActivity(
                        context,
                        0,
                        intent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );

        mBuilder.setContentIntent(resultPedingIntent);



        NotificationManager mNotifyMgr = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        mNotifyMgr.notify(_id, mBuilder.build());
    }

    private void updateWord(int id, int currentIsInRemindList) {
        ContentValues values = new ContentValues();

        if (currentIsInRemindList == WordContract.WordEntry.NO){

            Date date = new Date();
            Log.e(id + "", String.valueOf(date.getTime()));
            values.put(WordContract.WordEntry.COLUMN_REMIND_TIME, String.valueOf(date.getTime()));
            values.put(WordContract.WordEntry.COLUMN_IS_IN_REMIND_LIST,WordContract.WordEntry.YES);


        }
        if (currentIsInRemindList == WordContract.WordEntry.YES){
            values.put(WordContract.WordEntry.COLUMN_REMIND_TIME, "");
            values.put(WordContract.WordEntry.COLUMN_IS_IN_REMIND_LIST, WordContract.WordEntry.NO);
        }
        Uri currentWordUri = Uri.withAppendedPath(WordContract.WordEntry.WORDS_CONTENT_URI, String.valueOf(id));
        int updateRows = context.getContentResolver().update(currentWordUri,values, null, null);
        if (updateRows > -1){
            startAlarm(id);
        }
    }

    private void promtSpeechInput(int id, String englishTranslation) {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.ENGLISH);
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, context.getString(R.string.speech_prompt) + englishTranslation );
       try {
           context.startActivityForResult(intent, id);
       } catch (ActivityNotFoundException a){
           Toast.makeText(context, context.getString(R.string.speech_not_supported), Toast.LENGTH_SHORT).show();
       }
    }
}
