package com.example.admin.learningenglish.databases.vocabulary;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by zznam on 27-Oct-17.
 */

public class WordContract {

    public static final String CONTENT_AUTHORITY = "com.example.admin.learningenglish";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);
    public static final String PATH_WORDS = "words";
    public static final String PATH_REMIND_WORDS = "remindWords";
    public static final String PATH_WORDS_OF_LESSION = "wordsOfLession";
    public static abstract class WordEntry implements BaseColumns {
        public static final String TABLE_NAME = "vocab_words";
        public static final String COLUMN_WORD_ID = "_id";
        public static final String COLUMN_DEFAULT_TRANSLATION = "default_translation";
        public static final String COLUMN_VIETNAMESE_TRANSLATION = "vietnamese_translation";
        public static final String COLUMN_IPA_PRONUNCIATION = "pronunciation";
        public static final String COLUMN_EXAMPLE = "example";
        public static final String COLUMN_WORD_CLASS = "class";
        public static final String COLUMN_LESSION_ID = "lession_id";
        public static final String COLUMN_IMAGE_RES_ID = "image_res_id";
        public static final String COLUMN_AUDIO_RES_ID = "audio_res_id";
        public static final String COLUMN_IS_IN_REMIND_LIST = "is_in_remind_list";
        public static final String COLUMN_REMIND_TIME = "remind_time";

        public static final int NO_IMAGE_PROVIDED = -1;
        public static final int YES = 1;
        public static final int NO = 0;

        public static final Uri WORDS_CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, PATH_WORDS);
        public static final Uri WORDS_OF_LESSION_CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, PATH_WORDS_OF_LESSION);
        public static final Uri REMIND_WORDS_CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, PATH_REMIND_WORDS);
        public static final String WORDS_CONTENT_LIST_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_WORDS ;
        public static final String WORDS_CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_WORDS ;


    }
}
