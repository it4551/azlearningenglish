package com.example.admin.learningenglish.databases.vocabulary;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.example.admin.learningenglish.databases.vocabulary.LessionContract.LessionEntry;
import com.example.admin.learningenglish.databases.vocabulary.WordContract.WordEntry;

/**
 * Created by zznam on 11/3/2017.
 */

public class MyProvider extends ContentProvider {

    private static final int LESSIONS = 100;
    private static final int LESSION_ID = 101;
    private static final int WORDS = 201;
    private static final int WORDS_ID = 202;
    private static final int WORDS_BY_LESSION_ID = 301;
    private static final int REMIND_WORDS = 302;
    public static final String LOG_TAG = MyProvider.class.getSimpleName();
    private MyDbHelper mDbHelper;

    private static final UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    static {
        sUriMatcher.addURI(WordContract.CONTENT_AUTHORITY, WordContract.PATH_WORDS, WORDS);
        sUriMatcher.addURI(WordContract.CONTENT_AUTHORITY, WordContract.PATH_REMIND_WORDS, REMIND_WORDS);
        sUriMatcher.addURI(WordContract.CONTENT_AUTHORITY, WordContract.PATH_WORDS + "/#", WORDS_ID);
        sUriMatcher.addURI(LessionContract.CONTENT_AUTHORITY, LessionContract.PATH_LESSIONS, LESSIONS);
        sUriMatcher.addURI(LessionContract.CONTENT_AUTHORITY, LessionContract.PATH_LESSIONS + "/#", LESSION_ID);
        sUriMatcher.addURI(WordContract.CONTENT_AUTHORITY, WordContract.PATH_WORDS_OF_LESSION +"/#", WORDS_BY_LESSION_ID);
    }
    @Override
    public boolean onCreate() {

        mDbHelper = new MyDbHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        // Get readable database
        SQLiteDatabase database = mDbHelper.getReadableDatabase();
        // This cursor will hold the result of the query
        Cursor cursor;

        int match = sUriMatcher.match(uri);

        switch (match){
            case WORDS:
                cursor = database.query(WordEntry.TABLE_NAME, projection, selection, selectionArgs,
                        null, null, sortOrder);
                break;
            case WORDS_ID:
                selection = WordEntry._ID + "=?";
                selectionArgs = new String[] { String.valueOf(ContentUris.parseId(uri)) };
                cursor = database.query(WordEntry.TABLE_NAME, projection, selection, selectionArgs,
                        null, null, sortOrder);
                break;
            case LESSIONS:
                cursor = database.query(LessionEntry.TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);
                break;
            case LESSION_ID:
                selection = LessionEntry._ID + "=?";
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};
                cursor = database.query(LessionEntry.TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);
                break;
            case WORDS_BY_LESSION_ID:
                if (selection!=null){
                    selection = "(" +selection + ") AND " + WordEntry.COLUMN_LESSION_ID + "=?";
                    String[] selectionArgsNew = {selectionArgs[0],selectionArgs[1],selectionArgs[2],String. valueOf(ContentUris.parseId(uri))};
                    cursor = database.query(WordEntry.TABLE_NAME, projection, selection, selectionArgsNew, null, null, sortOrder);
                    Log.e("DBHELPER QUERY", selectionArgsNew[3]);
                } else {
                    selection = WordEntry.COLUMN_LESSION_ID + "=?";
                    selectionArgs =  new String[]{String. valueOf(ContentUris.parseId(uri))};
                    Log.e("DBHELPER QUERY", selectionArgs[0]);
                    cursor = database.query(WordEntry.TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);
                }
                break;
            case REMIND_WORDS:
                selection = WordEntry.COLUMN_IS_IN_REMIND_LIST + "=?";
                selectionArgs = new String[] { String.valueOf(WordEntry.YES) };
                cursor = database.query(WordEntry.TABLE_NAME, projection, selection, selectionArgs,
                        null, null, sortOrder);
                break;
            default:
                throw new IllegalArgumentException("Cannot query unknown URI " + uri);
        }

        cursor.setNotificationUri(getContext().getContentResolver(), uri);


        return cursor;


    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        int match = sUriMatcher.match(uri);
        switch (match){
            case WORDS_ID:
                return WordEntry.WORDS_CONTENT_ITEM_TYPE;
            case WORDS:
                return WordEntry.WORDS_CONTENT_LIST_TYPE;
            case LESSION_ID:
                return LessionEntry.LESSION_CONTENT_ITEM_TYPE;
            case LESSIONS:
                return LessionEntry.LESSION_CONTENT_LIST_TYPE;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri + " with match " + match);
        }
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        int match = sUriMatcher.match(uri);

        switch (match){
            case WORDS:

                return insertWord(uri, values);

            case LESSIONS:

                return insertLession(uri, values) ;

            default:
                throw new IllegalArgumentException("Cannot query unknown URI" + uri);
        }
    }

    private Uri insertLession(Uri uri, ContentValues values) {

//        String name = values.getAsString(LessionEntry.COLUMN_LESSION_NAME);
//        int level = values.getAsInteger(LessionEntry.COLUMN_LESSION_LEVEL);
//        int score = values.getAsInteger(LessionEntry.COLUMN_LESSION_SCORE);
//        int  numberOfWords = values.getAsInteger(LessionEntry.COLUMN_NUMBER_OF_WORDS);
//        String lastTimeAccessed = values.getAsString(LessionEntry.COLUMN_LAST_TIME_ACCESSED);



        // Get writeable database
        SQLiteDatabase database = mDbHelper.getWritableDatabase();

        // Insert the new pet with the given values
        long id = database.insert(LessionEntry.TABLE_NAME, null, values);
        // If the ID is -1, then the insertion failed. Log an error and return null.
        if (id == -1) {
            Log.e(LOG_TAG, "Failed to insert row for " + uri);
            return null;
        }

        // Notify all listeners that the data has changed for the pet content URI
        getContext().getContentResolver().notifyChange(uri, null);

        // Return the new URI with the ID (of the newly inserted row) appended at the end
        return ContentUris.withAppendedId(uri, id);
    }

    private Uri insertWord(Uri uri, ContentValues values) {
// Get writeable database
        SQLiteDatabase database = mDbHelper.getWritableDatabase();

        // Insert the new word with the given values
        long id = database.insert(WordEntry.TABLE_NAME, null, values);
        // If the ID is -1, then the insertion failed. Log an error and return null.
        if (id == -1) {
            Log.e(LOG_TAG, "Failed to insert row for " + uri);
            return null;
        }

        // Notify all listeners that the data has changed for the pet content URI
        getContext().getContentResolver().notifyChange(uri, null);

        // Return the new URI with the ID (of the newly inserted row) appended at the end
        return ContentUris.withAppendedId(uri, id);    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {


        //if there are no values to update, then don't try to update the database
        if (values.size() == 0) return 0;

        if (values.containsKey(WordEntry.COLUMN_DEFAULT_TRANSLATION)){
            String name = values.getAsString(WordEntry.COLUMN_DEFAULT_TRANSLATION);
            if (name == null){
                throw new IllegalArgumentException("Word requires an English translate");
            }
        }


        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        int match  = sUriMatcher.match(uri);
        int updateRows = 0;
        switch (match){
            case LESSION_ID:
                selection = LessionEntry._ID + "=?";
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};
                updateRows = db.update(LessionEntry.TABLE_NAME,values,selection, selectionArgs);
                if (updateRows !=0){
                    //notify all listeners that the data has changed for the pet content URI
                    getContext().getContentResolver().notifyChange(LessionEntry.LESSIONS_CONTENT_URI, null);
                }
                break;
            case WORDS:
                updateRows = db.update(WordEntry.TABLE_NAME,values,selection, selectionArgs);
                break;
            case WORDS_ID:
                selection = WordEntry._ID + "=?";
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};
                updateRows = db.update(WordEntry.TABLE_NAME,values,selection, selectionArgs);
                if (updateRows !=0){
                    //notify all listeners that the data has changed for the pet content URI
                    getContext().getContentResolver().notifyChange(WordEntry.WORDS_CONTENT_URI, null);
                    getContext().getContentResolver().notifyChange(WordEntry.REMIND_WORDS_CONTENT_URI, null);
                }
                break;
            default:
                throw new IllegalArgumentException("Cannot update unknown Uri");
        }


        return updateRows;
    }
}
