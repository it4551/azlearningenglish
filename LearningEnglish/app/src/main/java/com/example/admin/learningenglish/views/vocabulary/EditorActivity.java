package com.example.admin.learningenglish.views.vocabulary;

import android.app.AlertDialog;
import android.app.LoaderManager;
import android.content.ContentValues;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

import com.example.admin.learningenglish.R;
import com.example.admin.learningenglish.adapters.vocabulary.WordCursorAdapter;
import com.example.admin.learningenglish.databases.vocabulary.LessionContract.LessionEntry;
import com.example.admin.learningenglish.databases.vocabulary.WordContract;
import com.example.admin.learningenglish.databases.vocabulary.WordContract.WordEntry;

public class EditorActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {


    private Toolbar mToolbar;

    private EditText mEnglishWordEditText;
    private EditText mDefaultTranslationEditText;
    private EditText mExampleEditText;
    private EditText mWordClassEditText;
    private EditText mIpaPronunciationEditText;
    private Spinner mLessionSpinner;
    private Button btnPickAudio;
    private Button btnPickImage;

    private Uri currentWordUri;

    private WordCursorAdapter mWordCursorAdapter;
    private static final int EXISTING_WORD_LOADER = 28;

    private int mLession = 20;
    private boolean mWordHasChanged = false;
//    private MyDbHelper myDbHelper;
    private ArrayList<Integer> idLessionList;
    private ArrayList<String>  nameLessionList;
    private View.OnTouchListener mTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            mWordHasChanged = true;
            return false;
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editor);

//        mToolbar = (Toolbar) findViewById(R.id.toolbar_editor_activity);
//        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mEnglishWordEditText = (EditText) findViewById(R.id.edit_english_translation);
        mDefaultTranslationEditText = (EditText) findViewById(R.id.edit_default_translation);
        mExampleEditText = (EditText) findViewById(R.id.edit_example);
        mWordClassEditText = (EditText) findViewById(R.id.edit_word_class);
        mIpaPronunciationEditText = (EditText) findViewById(R.id.edit_ipa_pronunciation);
        mLessionSpinner = (Spinner) findViewById(R.id.spinner_lession);
        btnPickAudio = (Button) findViewById(R.id.btnPickAudio);
        btnPickImage = (Button) findViewById(R.id.btnPickImage);

        mEnglishWordEditText.setOnTouchListener(mTouchListener);
        mDefaultTranslationEditText.setOnTouchListener(mTouchListener);
        mExampleEditText.setOnTouchListener(mTouchListener);
        mWordClassEditText.setOnTouchListener(mTouchListener);
        mIpaPronunciationEditText.setOnTouchListener(mTouchListener);
        mLessionSpinner.setOnTouchListener(mTouchListener);
        btnPickAudio.setOnTouchListener(mTouchListener);
        btnPickImage.setOnTouchListener(mTouchListener);

        setupSpinner();

        Intent intent = getIntent();
        currentWordUri = intent.getData();
        if (currentWordUri != null){
            setTitle("Edit word");;
            Log.e("Edit word", currentWordUri.toString());

            //kick off the loader
            getLoaderManager().initLoader(EXISTING_WORD_LOADER, null, this);
        } else {
            setTitle("Add a word");

            invalidateOptionsMenu();
        }

//        myDbHelper = new MyDbHelper(this);


    }

    private void setupSpinner() {

        idLessionList = new ArrayList();
        nameLessionList = new ArrayList();
        String projection[] = {
                LessionEntry._ID,
                LessionEntry.COLUMN_LESSION_NAME,

        };
//        SQLiteDatabase database = myDbHelper.getReadableDatabase();
        Cursor cursor = getContentResolver().query(LessionEntry.LESSIONS_CONTENT_URI, projection, null, null, null);

        if (cursor.moveToFirst()){
            do {
                String lessionName = cursor.getString(cursor.getColumnIndexOrThrow(LessionEntry.COLUMN_LESSION_NAME));
                int lessionId = cursor.getInt(cursor.getColumnIndexOrThrow(LessionEntry._ID));
                idLessionList.add(lessionId);
                nameLessionList.add("Lession " + lessionId+ ": "+  lessionName );
            } while (cursor.moveToNext());
        }

        ArrayAdapter lessionSpinnerAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, nameLessionList);

        mLessionSpinner.setAdapter(lessionSpinnerAdapter);
        mLessionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mLession = (int) idLessionList.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                mLession = 20;
            }
        });



    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String projection[] = {
                WordEntry._ID,
                WordEntry.COLUMN_DEFAULT_TRANSLATION,
                WordEntry.COLUMN_VIETNAMESE_TRANSLATION,
                WordEntry.COLUMN_IPA_PRONUNCIATION,
                WordEntry.COLUMN_EXAMPLE,
                WordEntry.COLUMN_LESSION_ID ,
                WordEntry.COLUMN_WORD_CLASS,
                WordEntry.COLUMN_IMAGE_RES_ID,
                WordEntry.COLUMN_AUDIO_RES_ID,
                WordEntry.COLUMN_IS_IN_REMIND_LIST,
                WordEntry.COLUMN_REMIND_TIME
        };
        return new CursorLoader(this,
                currentWordUri,
                projection,
                null,
                null,
                null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        if (cursor == null || cursor.getCount() <1){
            return;
        }

        if (cursor.moveToFirst()){
             int _id = cursor.getInt(cursor.getColumnIndexOrThrow(WordContract.WordEntry._ID));
            String wordClass = cursor.getString(cursor.getColumnIndexOrThrow(WordContract.WordEntry.COLUMN_WORD_CLASS));
             String englishTranslation = cursor.getString(cursor.getColumnIndexOrThrow(WordContract.WordEntry.COLUMN_DEFAULT_TRANSLATION));
            String vietnameseTranslation = cursor.getString(cursor.getColumnIndexOrThrow(WordContract.WordEntry.COLUMN_VIETNAMESE_TRANSLATION));
            String ipaPronunciation = cursor.getString(cursor.getColumnIndexOrThrow(WordContract.WordEntry.COLUMN_IPA_PRONUNCIATION));
             String example = cursor.getString(cursor.getColumnIndexOrThrow(WordContract.WordEntry.COLUMN_EXAMPLE));
            int lessionId = cursor.getInt(cursor.getColumnIndexOrThrow(WordEntry.COLUMN_LESSION_ID));
            int imageResourceId = cursor.getInt(cursor.getColumnIndexOrThrow(WordContract.WordEntry.COLUMN_IMAGE_RES_ID));
            int audioResourceId = cursor.getInt(cursor.getColumnIndexOrThrow(WordContract.WordEntry.COLUMN_AUDIO_RES_ID));
             int isInRemindList = cursor.getInt(cursor.getColumnIndexOrThrow(WordContract.WordEntry.COLUMN_IS_IN_REMIND_LIST));

             mEnglishWordEditText.setText(englishTranslation);
            mDefaultTranslationEditText.setText(vietnameseTranslation);
            mIpaPronunciationEditText.setText(ipaPronunciation);
            mExampleEditText.setText(example);
            mWordClassEditText.setText(wordClass);
            mEnglishWordEditText.setText(englishTranslation);
            for (int i=0;i< idLessionList.size();i++){
                if (lessionId == idLessionList.get(i)){
                    mLessionSpinner.setSelection(i);
                    break;
                }
            }

        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mEnglishWordEditText.setText("");
        mDefaultTranslationEditText.setText("");
        mIpaPronunciationEditText.setText("");
        mExampleEditText.setText("");
        mWordClassEditText.setText("");
        mLessionSpinner.setSelection(0);
//        mWordCursorAdapter.swapCursor(null);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_editor, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
         super.onPrepareOptionsMenu(menu);
         if(currentWordUri == null){
             MenuItem menuItem = menu.findItem(R.id.action_delete);
             menuItem.setVisible(false);
         }
         return  true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_save:
                if(TextUtils.isEmpty(mEnglishWordEditText.getText())
                       ){
                    finish();
                    return false;
                }
                if (currentWordUri == null){
                    //save Pet to database
                    insertWord();
                } else {
                    updateWord();
                }
                //exit activity
                finish();
                return true;
            case R.id.action_delete:
                return true;
            case android.R.id.home:
                if (!mWordHasChanged){
                    NavUtils.navigateUpFromSameTask(this);
                    return true;
                }


                //otherwise if there are unsaved changes, setup a dialog to warn the user.
                //create a click listener to handle the user confirming that changes should be discarded

                DialogInterface.OnClickListener discardButtonClickListener =
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //user click "discard" button, close the current activity.
                                finish();
                            }
                        };
                showUnsavedChangesDialog(discardButtonClickListener);
                return true;
        }


        return super.onOptionsItemSelected(item);
    }

    private void updateWord() {
        String englishWord = mEnglishWordEditText.getText().toString().trim();
        String defaultTranslation = mDefaultTranslationEditText.getText().toString();
        String ipaPronunciation = mIpaPronunciationEditText.getText().toString();
        String example = mExampleEditText.getText().toString();
        String wordClass = mWordClassEditText.getText().toString();
        ContentValues values = new ContentValues();
        values.put(WordEntry.COLUMN_WORD_CLASS, wordClass);
        values.put(WordEntry.COLUMN_DEFAULT_TRANSLATION, englishWord);
        values.put(WordEntry.COLUMN_VIETNAMESE_TRANSLATION, defaultTranslation);
        values.put(WordEntry.COLUMN_IPA_PRONUNCIATION, ipaPronunciation);
        values.put(WordEntry.COLUMN_EXAMPLE,example);
        values.put(WordEntry.COLUMN_LESSION_ID, mLession);
//        values.put(WordEntry.COLUMN_IMAGE_RES_ID, word.getImageResourceId());
//        values.put(WordEntry.COLUMN_AUDIO_RES_ID, word.getAudioResourceId());
        values.put(WordEntry.COLUMN_IS_IN_REMIND_LIST, WordEntry.YES);

        int updateRows= getContentResolver().update(currentWordUri, values,null, null);
        if (updateRows == 0){
            Toast.makeText(this, getString(R.string.editor_insert_word_failed), Toast.LENGTH_SHORT).show();

        } else {
            Toast.makeText(this, getString(R.string.editor_update_word_successful) , Toast.LENGTH_SHORT).show();
        }
    }

    private void insertWord() {
        String englishWord = mEnglishWordEditText.getText().toString().trim();
        String defaultTranslation = mDefaultTranslationEditText.getText().toString();
        String ipaPronunciation = mIpaPronunciationEditText.getText().toString();
        String example = mExampleEditText.getText().toString();
        String wordClass = mWordClassEditText.getText().toString();
        ContentValues values = new ContentValues();
        values.put(WordEntry.COLUMN_WORD_CLASS, wordClass);
        values.put(WordEntry.COLUMN_DEFAULT_TRANSLATION, englishWord);
        values.put(WordEntry.COLUMN_VIETNAMESE_TRANSLATION, defaultTranslation);
        values.put(WordEntry.COLUMN_IPA_PRONUNCIATION, ipaPronunciation);
        values.put(WordEntry.COLUMN_EXAMPLE,example);
        values.put(WordEntry.COLUMN_LESSION_ID, mLession);
//        values.put(WordEntry.COLUMN_IMAGE_RES_ID, word.getImageResourceId());
//        values.put(WordEntry.COLUMN_AUDIO_RES_ID, word.getAudioResourceId());
        values.put(WordEntry.COLUMN_IS_IN_REMIND_LIST, WordEntry.YES);

        Uri uri = WordEntry.WORDS_CONTENT_URI;
        Uri newUri= getContentResolver().insert(uri, values);
        if (newUri== null){
            Toast.makeText(this, getString(R.string.editor_insert_word_failed), Toast.LENGTH_SHORT).show();

        } else {
            Toast.makeText(this, getString(R.string.editor_insert_word_successful) , Toast.LENGTH_SHORT).show();
        }
    }

    private void showUnsavedChangesDialog(DialogInterface.OnClickListener discardButtonClickListener) {
        // create an AlertDialog.Builder and set the message and click listeners for the positive and negative buttons on the dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.unsaved_changes_dialog_msg);

        builder.setPositiveButton(R.string.discard, discardButtonClickListener);
        builder.setNegativeButton(R.string.keep_editing, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (dialog != null){
                    dialog.dismiss();
                }
            }
        });
        // Create and show the AlertDialog
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

}
