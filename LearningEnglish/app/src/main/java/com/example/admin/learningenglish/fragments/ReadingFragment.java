package com.example.admin.learningenglish.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.example.admin.learningenglish.adapters.ReadingAdapter;
import com.example.admin.learningenglish.databases.Database;
import com.example.admin.learningenglish.views.ExerciseReadingActivity;
import com.example.admin.learningenglish.views.MainActivity;
import com.example.admin.learningenglish.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;

/**
 * Created by admin on 10/7/2017.
 */

public class ReadingFragment extends Fragment {
    private ArrayList<String> ten;
    private ListView lvReading;
    private ArrayList<String> dsDiem = new ArrayList<>();
    private ArrayList<String> dsTongDiem = new ArrayList<>();
    private String baiDangHoc = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reading,container,false);

        addControl(view);
        getDatabase();
        khoiTao();
        addEvent();

        return view;
    }

    private void getDatabase() {
        Database database = new Database(getActivity());
        Cursor cursorListenResponse = database.getData("select ten from doc");
        ten = new ArrayList<>();
        while (cursorListenResponse.moveToNext()) {
            ten.add(cursorListenResponse.getString(0));
        }
    }

    private void khoiTao() {
        getScore();
        ReadingAdapter readingAdapter = new ReadingAdapter(getActivity(),ten,dsDiem,dsTongDiem);
        lvReading.setAdapter(readingAdapter);
    }

    private void getScore() {
        Database database = new Database(getActivity());

        //lấy điểm
        String sqlDiem = "select diem from doc";
        Cursor cursorDiem = database.getData(sqlDiem);
        dsDiem = new ArrayList<>();
        while (cursorDiem.moveToNext()) {
            dsDiem.add(cursorDiem.getString(0));
        }

        //lấy tổng điểm
        String sqlTongDiem = "select count(cauhoi.diem) from doc, bai, cauhoi where doc.id = bai.iddoc and " +
                "bai.id = cauhoi.idbai group by doc.id";
        Cursor cursorTongDiem = database.getData(sqlTongDiem);
        dsTongDiem = new ArrayList<>();
        while (cursorTongDiem.moveToNext()) {
            dsTongDiem.add(cursorTongDiem.getString(0));
        }
    }

    private void addEvent() {
        lvReading.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getActivity(), ExerciseReadingActivity.class);
                String baiHoc = String.valueOf(i + 1);
                intent.putExtra("baihoc",baiHoc);
                startActivity(intent);
            }
        });

        MainActivity.btnHocTiep.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String baiHoc = getBaiHoc();
                Intent intent = new Intent(getActivity(), ExerciseReadingActivity.class);
                intent.putExtra("baihoc",baiHoc);
                startActivity(intent);
            }
        });
    }

    private String getBaiHoc() {
        String baiHoc = "";
        for (int i = 0 ; i < ten.size() ; i++) {
            if (ten.get(i).equals(baiDangHoc)) {
                baiHoc = String.valueOf(i + 1);
                break;
            }
        }

        return baiHoc;
    }

    private void addControl(View view) {
        lvReading = (ListView) view.findViewById(R.id.lvReading);
    }

    @Override
    public void onResume() {
        super.onResume();
        MainActivity.txtTitle.setText("Reading");

        readStatus();

        if (!baiDangHoc.equals("")) {
            MainActivity.rlActionbarBot.setVisibility(View.VISIBLE);
        }
        else {
            MainActivity.rlActionbarBot.setVisibility(View.GONE);
        }

        MainActivity.txtBaiHoc.setText("Đang học : \"" + baiDangHoc + "\"");
        khoiTao();
    }

    private void readStatus() {
        //tạo folder chua file
        File myFile = new File (Environment.getExternalStorageDirectory() + "/LearningEnglish/Reading");
        if (!myFile.exists()){
            myFile.mkdirs();
        }

        try {
            FileInputStream fis = new FileInputStream(myFile.getAbsolutePath() + "/Status");
            ObjectInputStream ois = new ObjectInputStream(fis);
            baiDangHoc = (String) ois.readObject();

            ois.close();
            fis.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
