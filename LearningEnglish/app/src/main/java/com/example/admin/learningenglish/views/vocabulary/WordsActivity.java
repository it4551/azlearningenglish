package com.example.admin.learningenglish.views.vocabulary;

import android.annotation.TargetApi;
import android.app.LoaderManager;
import android.app.SearchManager;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import com.example.admin.learningenglish.R;
import com.example.admin.learningenglish.adapters.vocabulary.WordCursorAdapter;
import com.example.admin.learningenglish.databases.vocabulary.WordContract;
import com.example.admin.learningenglish.databases.vocabulary.WordContract.WordEntry;

public class WordsActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    private ListView wordListView;
    private View emptyView;
    private Toolbar mToolbar;

    private static final int WORD_LOADER_ID = 21;
    private WordCursorAdapter mWordCursorAdapter;
    private Uri currentWordsOfLessionUri;
    AudioManager mAudioManager;
    private MediaPlayer mMediaPlayer;
    private MediaPlayer.OnCompletionListener mCompletionListener = new MediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mp) {
            releaseMediaPlayer();
        }
    };
    public void releaseMediaPlayer(){
        if (mMediaPlayer != null){
            mMediaPlayer.release();
            mMediaPlayer = null;
            mAudioManager.abandonAudioFocus(l);

        }
    }

    AudioManager.OnAudioFocusChangeListener l = new AudioManager.OnAudioFocusChangeListener() {
        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public void onAudioFocusChange(int focusChange) {
            if (focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT){
                mMediaPlayer.pause();
                mMediaPlayer.seekTo(0);
                Log.w(getLocalClassName(),mMediaPlayer.getTrackInfo().toString());
            } else if (focusChange == AudioManager.AUDIOFOCUS_LOSS){
                mMediaPlayer.stop();
                releaseMediaPlayer();
                Log.w(getLocalClassName(),mMediaPlayer.getTrackInfo().toString());
            } else if (focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK){
                mMediaPlayer.pause();
                mMediaPlayer.seekTo(0);
                Log.w(getLocalClassName(),mMediaPlayer.getTrackInfo().toString());
            } else if (focusChange == AudioManager.AUDIOFOCUS_GAIN){
                mMediaPlayer.start();
                Log.w(getLocalClassName(),mMediaPlayer.getTrackInfo().toString());
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_words);
//        mToolbar = (Toolbar) findViewById(R.id.toolbar_words_activity);
//        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        currentWordsOfLessionUri = intent.getData();
        if(currentWordsOfLessionUri==null){
            currentWordsOfLessionUri = Uri.withAppendedPath(WordContract.WordEntry.WORDS_OF_LESSION_CONTENT_URI,"1");
        } else {
            Log.v(this.getLocalClassName(),currentWordsOfLessionUri.toString()+" ");

        }

//        ArrayList<VocabWord> wordsList = new ArrayList<>();
//        wordsList.add(new VocabWord("number","one","một","wən","there's only room for one person",1, R.drawable.number_one));
//        wordsList.add(new VocabWord("number","two","hai","to͞o","two years ago",1, R.drawable.number_two));

        wordListView = (ListView) findViewById(R.id.wordListView);
        emptyView = findViewById(R.id.empty_view);
        wordListView.setEmptyView(emptyView);
//        ArrayAdapter<VocabWord> arrayAdapter = new ArrayAdapter<VocabWord>(this,android.R.layout.simple_list_item_1, wordsList);
        mWordCursorAdapter  = new WordCursorAdapter(this, null);
        wordListView.setAdapter(mWordCursorAdapter);


        wordListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.e(getLocalClassName(), "Current Word:" + position);
                Cursor cursor = mWordCursorAdapter.getCursor();
                cursor.moveToPosition((int) position);

                int audioResourceId = cursor.getInt(cursor.getColumnIndexOrThrow(WordEntry.COLUMN_AUDIO_RES_ID));
                Log.e(getLocalClassName(), "Current audioResourceId:" + audioResourceId);
                releaseMediaPlayer();
                mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                int result =  mAudioManager.requestAudioFocus(l, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);
                if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED && audioResourceId != -1){
                    mMediaPlayer = MediaPlayer.create(WordsActivity.this, audioResourceId);
                    mMediaPlayer.start();
                    mMediaPlayer.setOnCompletionListener(mCompletionListener);
                }
            }
        });

        //        Intent
        //kick off the loader
        getLoaderManager().initLoader(WORD_LOADER_ID, null, this);


    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
//        Uri uri = WordEntry.WORDS_CONTENT_URI;
        String projection[] = {
                WordEntry._ID,
                WordEntry.COLUMN_DEFAULT_TRANSLATION,
                WordEntry.COLUMN_VIETNAMESE_TRANSLATION,
                WordEntry.COLUMN_IPA_PRONUNCIATION,
                WordEntry.COLUMN_EXAMPLE,
                WordEntry.COLUMN_LESSION_ID ,
                WordEntry.COLUMN_WORD_CLASS,
                WordEntry.COLUMN_IMAGE_RES_ID,
                WordEntry.COLUMN_AUDIO_RES_ID,
                WordEntry.COLUMN_IS_IN_REMIND_LIST,
                WordEntry.COLUMN_REMIND_TIME
        };
        String selection = null;
        String[] selectionArgs = null;
        if(args != null){
            selection = WordEntry.COLUMN_VIETNAMESE_TRANSLATION + " like ?" +" OR " + WordEntry.COLUMN_DEFAULT_TRANSLATION + " like ?" + " OR " + WordEntry.COLUMN_WORD_CLASS + " like ?";
            selectionArgs = new String[] {"%" + args.getString("SEARCH_QUERY") + "%", "%" + args.getString("SEARCH_QUERY") + "%", "%" + args.getString("SEARCH_QUERY") + "%"};
        }
        return new CursorLoader(this, currentWordsOfLessionUri, projection, selection, selectionArgs, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mWordCursorAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mWordCursorAdapter.swapCursor(null);

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_activity_words, menu);


        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        final SearchView searchView = (SearchView) menu.findItem(R.id.action_search_words_act).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchView.clearFocus();
                Bundle bundle = new Bundle();
                bundle.putString("SEARCH_QUERY",query);
                if (query!=null && query.length()!=0){
                    getLoaderManager().restartLoader(WORD_LOADER_ID, bundle, WordsActivity.this);
                }
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                Bundle bundle = new Bundle();
                bundle.putString("SEARCH_QUERY",newText);
                getLoaderManager().restartLoader(WORD_LOADER_ID, bundle, WordsActivity.this);

                return true;            }
        });
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings_words_act){
            Log.e(getLocalClassName(), getString(R.string.action_settings));
            return true;
        }
        if (id == R.id.action__share_words_act){
            Log.e(getLocalClassName(), getString(R.string.action_share));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ArrayList<String>  result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
        // todo update score of word and lession
        if (result.get(0)!=null){
            Toast.makeText(this, getString(R.string.you_ve_prnounced) + result.get(0), Toast.LENGTH_SHORT).show();
        }
    }
}
