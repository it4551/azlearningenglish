package com.example.admin.learningenglish.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.admin.learningenglish.views.MainActivity;
import com.example.admin.learningenglish.R;

/**
 * Created by admin on 10/7/2017.
 */

public class SettingFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_setting,container,false);

        addControl(view);
        addEvent();

        return view;
    }

    private void addEvent() {
    }

    private void addControl(View view) {
    }

    @Override
    public void onResume() {
        super.onResume();
        MainActivity.txtTitle.setText("Setting");
    }
}
