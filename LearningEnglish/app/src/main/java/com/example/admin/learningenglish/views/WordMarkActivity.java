package com.example.admin.learningenglish.views;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.admin.learningenglish.R;

public class WordMarkActivity extends AppCompatActivity {
    TextView txtTitle;
    ImageView imgBack, imgChoose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_word_mark);
        addActionBar();
        addControl();
        addEvent();
    }

    private void addActionBar() {
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.actionbar_custom_2);
        txtTitle = (TextView) findViewById(R.id.txtTitle);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        imgChoose = (ImageView) findViewById(R.id.imgChoose);

        txtTitle.setText("Work Mark");
        imgChoose.setImageResource(R.drawable.delete);
    }

    private void addEvent() {
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        imgChoose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //xóa
            }
        });
    }

    private void addControl() {
    }
}
