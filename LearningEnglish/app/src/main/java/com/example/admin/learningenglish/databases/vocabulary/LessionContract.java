package com.example.admin.learningenglish.databases.vocabulary;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by zznam on 11/3/2017.
 */

public class LessionContract {
    public  LessionContract(){

    }

    public static final String CONTENT_AUTHORITY =  "com.example.admin.learningenglish";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);
    public static final String PATH_LESSIONS = "lessions";
    public static final int EASY = 0;
    public static final int MEDIUM = 1;
    public static final int HARD = 2;
    public static abstract class LessionEntry implements BaseColumns{
        public static final String TABLE_NAME = "lessions";

        public final static String _ID = BaseColumns._ID;
        public static final String COLUMN_LESSION_NAME = "name";
        public static final String COLUMN_LESSION_LEVEL = "level";
        public static final String COLUMN_LESSION_SCORE = "score";
        public static final String COLUMN_NUMBER_OF_WORDS = "number_of_words";
        public static final String COLUMN_LAST_TIME_ACCESSED = "last_time_accessed";


        public static final Uri LESSIONS_CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, PATH_LESSIONS);

        public static final String LESSION_CONTENT_LIST_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_LESSIONS ;
        public static final String LESSION_CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_LESSIONS ;

    }

}
