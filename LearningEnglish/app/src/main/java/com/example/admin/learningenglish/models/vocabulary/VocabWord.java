package com.example.admin.learningenglish.models.vocabulary;

/**
 * Created by zznam on 24-Oct-17.
 */

public class VocabWord {
    private int mWordID;
    private String mWordClass;
    private String mDefaultTranslation;
    private String mVietnameseTranslation;
    private String mIpaPronunciation;
    private String mExample;
    private int mLessionID;

    private int mAudioResourceId;
    private int mImageResourceId = NO_IMAGE_PROVIDED;
    private static final int NO_IMAGE_PROVIDED = -1;

    private String mRemindTime;
    private int mIsInRemindList;
    private static final int YES = 1;
    private static final int NO = 0;

    public VocabWord( String mWordClass, String mDefaultTranslation, String mVietnameseTranslation, String mIpaPronunciation, String mExample, int mLessionID, int mImageResourceId, int audioResourceId, int isInRemindList) {
        this.mWordClass = mWordClass;
        this.mDefaultTranslation = mDefaultTranslation;
        this.mVietnameseTranslation = mVietnameseTranslation;
        this.mIpaPronunciation = mIpaPronunciation;
        this.mExample = mExample;
        this.mLessionID = mLessionID;
        this.mImageResourceId = mImageResourceId;
        this.mAudioResourceId = audioResourceId;
        this.mIsInRemindList = isInRemindList;
    }

    public int getWordID() {
        return mWordID;
    }

    public String getWordClass() {
        return mWordClass;
    }

    public String getDefaultTranslation() {
        return mDefaultTranslation;
    }

    public String getVietnameseTranslation() {
        return mVietnameseTranslation;
    }

    public String getIpaPronounciation() {
        return mIpaPronunciation;
    }

    public String getExample() {
        return mExample;
    }

    public int getLessionID() {
        return mLessionID;
    }

    public int getImageResourceId() {
        return mImageResourceId;
    }

    public int getAudioResourceId() {
        return mAudioResourceId;
    }

    public int getIsInRemindList() {
        return mIsInRemindList;
    }

    public String getRemindTime() {
        return mRemindTime;
    }
}
