package com.example.admin.learningenglish.models.vocabulary;

/**
 * Created by zznam on 23-Oct-17.
 */

public class Lession {
    private int idLession;
    private String name;
    private int level;
    private int score = 0;
    private int numberOfWords;
    private String lastTimeAccess;

    public static final int EASY_LEVEL = 0;
    public static final int MEDIUM_LEVEL = 1;
    public static final int INTERMEDIATE_LEVEL = 2;

    public Lession(int idLession, String name, int level) {
        this.idLession = idLession;
        this.name = name;
        this.level = level;
    }

    public Lession(int id, String name, int level, String lastTimeAccess) {
        this.idLession = id;
        this.name = name;
        this.level = level;
        this.lastTimeAccess = lastTimeAccess;
    }

    public int getIdLession() {
        return idLession;
    }

    public String getName() {
        return name;
    }

    public int getLevel() {
        return level;
    }

    public String getLastTimeAccess() {
        return lastTimeAccess;
    }

    public int getScore() {
        return score;
    }

    public int getNumberOfWords() {
        return numberOfWords;
    }
}
