package com.example.admin.learningenglish.fragments;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.example.admin.learningenglish.adapters.MainAdapter;
import com.example.admin.learningenglish.views.MainActivity;
import com.example.admin.learningenglish.R;
import com.example.admin.learningenglish.views.vocabulary.VocabularyActivity;

/**
 * Created by admin on 10/7/2017.
 */

public class MainFragment extends Fragment {
    private FragmentTransaction transaction;
    private GridView gvTrangChu;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main,container,false);

        addControl(view);
        addEvent();
        khoiTao();

        return view;
    }

    private void khoiTao() {
        String[] ten = {
                "Dictionary",
                "Vocabulary",
                "Listen",
                "Listen - Response",
                "Grammar",
                "Reading"
        };

        int[] hinh = {
                R.drawable.dictionary,
                R.drawable.vocabulary,
                R.drawable.listen_sub,
                R.drawable.listen_response,
                R.drawable.grammar,
                R.drawable.reading
        };

        MainAdapter mainAdapter = new MainAdapter(getActivity(),ten,hinh);
        gvTrangChu.setAdapter(mainAdapter);
    }

    private void addEvent() {
        gvTrangChu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0 :
                        Fragment dictionaryFragment = new DictionaryFragment();
                        transaction = getFragmentManager().beginTransaction();
                        transaction.replace(R.id.container, dictionaryFragment);
                        transaction.addToBackStack(null);
                        transaction.commit();
                        break;
                    case 1:
//                        Fragment vocabularyFragment = new VocabularyFragment();
//                        transaction = getFragmentManager().beginTransaction();
//                        transaction.replace(R.id.container, vocabularyFragment);
//                        transaction.addToBackStack(null);
//                        transaction.commit();
                        Intent intent = new Intent(gvTrangChu.getContext(), VocabularyActivity.class);
                        startActivity(intent);
                        break;
                    case 2:
                        Fragment listenFragment = new ListenFragment();
                        transaction = getFragmentManager().beginTransaction();
                        transaction.replace(R.id.container, listenFragment);
                        transaction.addToBackStack(null);
                        transaction.commit();
                        break;
                    case 3:
                        Fragment listenResponseFragment = new ListenResponseFragment();
                        transaction = getFragmentManager().beginTransaction();
                        transaction.replace(R.id.container, listenResponseFragment);
                        transaction.addToBackStack(null);
                        transaction.commit();
                        break;
                    case 4:
                        Fragment grammarFragment = new GrammarFragment();
                        transaction = getFragmentManager().beginTransaction();
                        transaction.replace(R.id.container, grammarFragment);
                        transaction.addToBackStack(null);
                        transaction.commit();
                        break;
                    case 5:
                        Fragment readingFragment = new ReadingFragment();
                        transaction = getFragmentManager().beginTransaction();
                        transaction.replace(R.id.container, readingFragment);
                        transaction.addToBackStack(null);
                        transaction.commit();
                        break;
                }
            }
        });
    }

    private void addControl(View view) {
        gvTrangChu = (GridView) view.findViewById(R.id.gvTrangChu);
    }

    @Override
    public void onResume() {
        super.onResume();
        MainActivity.txtTitle.setText("Learning English");
        MainActivity.rlActionbarBot.setVisibility(View.GONE);
    }
}
