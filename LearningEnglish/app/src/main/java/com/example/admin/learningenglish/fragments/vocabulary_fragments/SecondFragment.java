package com.example.admin.learningenglish.fragments.vocabulary_fragments;


import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.admin.learningenglish.R;
import com.example.admin.learningenglish.adapters.vocabulary.WordCursorAdapter;
import com.example.admin.learningenglish.databases.vocabulary.WordContract.WordEntry;
import com.example.admin.learningenglish.views.vocabulary.EditorActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class SecondFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {
    private ListView wordListView;
    private View emptyView;
    private WordCursorAdapter mWordCursorAdapter;
    private static final int REMIND_WORD_LOADER_ID = 21;
    AudioManager mAudioManager;
    private MediaPlayer mMediaPlayer;


    public void releaseMediaPlayer(){
        if (mMediaPlayer != null){
            mMediaPlayer.release();
            mMediaPlayer = null;
            mAudioManager.abandonAudioFocus(l);

        }
    }
    MediaPlayer.OnCompletionListener mCompletionListener = new MediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mp) {
            releaseMediaPlayer();
        }
    };
    AudioManager.OnAudioFocusChangeListener l = new AudioManager.OnAudioFocusChangeListener() {
        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public void onAudioFocusChange(int focusChange) {
            if (focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT){
                mMediaPlayer.pause();
                mMediaPlayer.seekTo(0);
                Log.w("onAudioFocusChange",mMediaPlayer.getTrackInfo().toString());
            } else if (focusChange == AudioManager.AUDIOFOCUS_LOSS){
                mMediaPlayer.stop();
                releaseMediaPlayer();
                Log.w("onAudioFocusChange",mMediaPlayer.getTrackInfo().toString());
            } else if (focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK){
                mMediaPlayer.pause();
                mMediaPlayer.seekTo(0);
                Log.w("onAudioFocusChange",mMediaPlayer.getTrackInfo().toString());
            } else if (focusChange == AudioManager.AUDIOFOCUS_GAIN){
                mMediaPlayer.start();
                Log.w("onAudioFocusChange",mMediaPlayer.getTrackInfo().toString());
            }
        }
    };
    public SecondFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_second, container, false);

        // Setup FAB to open EditorActivity
        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab_second_fragment);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), EditorActivity.class);
                startActivity(intent);
            }
        });

        wordListView = (ListView) view.findViewById(R.id.remindWordListView);
        emptyView = view.findViewById(R.id.empty_view_2nd_fragment);
        wordListView.setEmptyView(emptyView);
        mWordCursorAdapter  = new WordCursorAdapter(this.getContext(), null);
        wordListView.setAdapter(mWordCursorAdapter);

        wordListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.e(this.getClass().getSimpleName(), "Current Word:" + position);
                Cursor cursor = mWordCursorAdapter.getCursor();
                cursor.moveToPosition((int) position);

                int audioResourceId = cursor.getInt(cursor.getColumnIndexOrThrow(WordEntry.COLUMN_AUDIO_RES_ID));
                Log.e(this.getClass().getSimpleName(), "Current audioResourceId:" + audioResourceId);
                releaseMediaPlayer();
                mAudioManager = (AudioManager) view.getContext().getSystemService(Context.AUDIO_SERVICE);
                int result =  mAudioManager.requestAudioFocus(l, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);
                if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED && audioResourceId!=-1){
                    mMediaPlayer = MediaPlayer.create(view.getContext(), audioResourceId);
                    mMediaPlayer.start();
                    mMediaPlayer.setOnCompletionListener(mCompletionListener);
                }
            }
        });
        //kick off the loader
        getLoaderManager().initLoader(REMIND_WORD_LOADER_ID, null, this);

        return view;
    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String projection[] = {
                WordEntry._ID,
                WordEntry.COLUMN_DEFAULT_TRANSLATION,
                WordEntry.COLUMN_VIETNAMESE_TRANSLATION,
                WordEntry.COLUMN_IPA_PRONUNCIATION,
                WordEntry.COLUMN_EXAMPLE,
                WordEntry.COLUMN_LESSION_ID ,
                WordEntry.COLUMN_WORD_CLASS,
                WordEntry.COLUMN_IMAGE_RES_ID,
                WordEntry.COLUMN_AUDIO_RES_ID,
                WordEntry.COLUMN_IS_IN_REMIND_LIST,
                WordEntry.COLUMN_REMIND_TIME
        };
        String selection = null;
        String[] selectionArgs = null;
        return new CursorLoader(this.getContext(),WordEntry.REMIND_WORDS_CONTENT_URI , projection, selection, selectionArgs, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mWordCursorAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mWordCursorAdapter.swapCursor(null);
    }
}
