package com.example.admin.learningenglish.views.vocabulary;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import com.example.admin.learningenglish.R;
import com.example.admin.learningenglish.databases.vocabulary.WordContract;
import com.example.admin.learningenglish.fragments.vocabulary_fragments.FirstFragment;
import com.example.admin.learningenglish.fragments.vocabulary_fragments.SecondFragment;
import com.example.admin.learningenglish.fragments.vocabulary_fragments.ThirdFragment;

public class VocabularyActivity extends AppCompatActivity {
    private static final int FIRSTFRAGMENT = 0;
    private static final int SECONDFRAGMENT = 1;
    private static final int THIRDFRAGMENT = 2;
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private FirstFragment firstFragment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vocabulary);

//        toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        firstFragment = new FirstFragment();
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                Log.e(getLocalClassName(),viewPager.getCurrentItem()+"");
                getContentResolver().notifyChange(WordContract.WordEntry.WORDS_CONTENT_URI, null);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }





    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(firstFragment,getResources().getString(R.string.lessions));
        adapter.addFragment(new SecondFragment(),getResources().getString(R.string.remind_words));
        adapter.addFragment(new ThirdFragment(),getResources().getString(R.string.flashcards));
        viewPager.setAdapter(adapter);
    }
    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        public List<Fragment> getFragmentList(){
            return mFragmentList;
        }
        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_activity_main, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        final SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
//        searchView.setSubmitButtonEnabled(true);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchView.clearFocus();
                Bundle bundle = new Bundle();
                bundle.putString("SEARCH_QUERY",query);
                if (viewPager.getCurrentItem()== FIRSTFRAGMENT && query!=null && query.length()!=0){
                    firstFragment.getLoaderManager().restartLoader(FirstFragment.LESSION_LOADER, bundle, firstFragment);
                }
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                Bundle bundle = new Bundle();
                bundle.putString("SEARCH_QUERY",newText);
                if (viewPager.getCurrentItem()== FIRSTFRAGMENT && newText!=null && newText.length()!=0){
                    firstFragment.getLoaderManager().restartLoader(FirstFragment.LESSION_LOADER, bundle, firstFragment);
                }
                return true;            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings){
            Log.e(getLocalClassName(), getString(R.string.action_settings));
            return true;
        }
        if (id == R.id.action_share){
            Log.e(getLocalClassName(), getString(R.string.action_share));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ArrayList<String>  result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
        // todo update score of word and lession
        if (result.get(0)!=null){
            Toast.makeText(this, getString(R.string.you_ve_prnounced) + result.get(0), Toast.LENGTH_SHORT).show();
        }
    }
}
