package com.example.admin.learningenglish.services;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;

import com.example.admin.learningenglish.R;
import com.example.admin.learningenglish.databases.vocabulary.WordContract;
import com.example.admin.learningenglish.views.vocabulary.EditorActivity;

/**
 * Created by zznam on 11/29/2017.
 */

public class ReminderService extends IntentService {

    public ReminderService() {
        super("Reminder Service");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        Uri currentWordUri = intent.getData();
        String projection[] = {
                WordContract.WordEntry._ID,
                WordContract.WordEntry.COLUMN_DEFAULT_TRANSLATION,
                WordContract.WordEntry.COLUMN_WORD_CLASS,
                WordContract.WordEntry.COLUMN_EXAMPLE
        };
        Cursor cursor = getContentResolver().query(
                currentWordUri,
                projection,
                null,
                null,
                null);
        if (cursor.moveToFirst()) {
            int id = cursor.getInt(cursor.getColumnIndexOrThrow(WordContract.WordEntry._ID));
            String engTranslation = cursor.getString(cursor.getColumnIndexOrThrow(WordContract.WordEntry.COLUMN_DEFAULT_TRANSLATION));
            String wordClass = cursor.getString(cursor.getColumnIndexOrThrow(WordContract.WordEntry.COLUMN_WORD_CLASS));
            String example = cursor.getString(cursor.getColumnIndexOrThrow(WordContract.WordEntry.COLUMN_EXAMPLE));

            testNotify(id, engTranslation, wordClass, example);
        }
    }
    private void testNotify(int _id, String englishTranslation, String wordClass, String example) {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_notifications_paused_black_24dp)
                        .setContentTitle(englishTranslation + " - (" + wordClass + ")")
                        .setContentText(example)
                        .setTicker(englishTranslation)
                ;
        Intent intent = new Intent(this, EditorActivity.class);
        intent.setData(Uri.withAppendedPath(WordContract.WordEntry.WORDS_CONTENT_URI, String.valueOf(_id)));
        PendingIntent resultPedingIntent =
                PendingIntent.getActivity(
                        getApplicationContext(),
                        0,
                        intent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );

        mBuilder.setContentIntent(resultPedingIntent);



        NotificationManager mNotifyMgr = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        mNotifyMgr.notify(_id, mBuilder.build());
    }

}
