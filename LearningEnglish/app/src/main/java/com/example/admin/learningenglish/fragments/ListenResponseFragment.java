package com.example.admin.learningenglish.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.admin.learningenglish.adapters.ListenAdapter;
import com.example.admin.learningenglish.adapters.ListenResponseAdapter;
import com.example.admin.learningenglish.databases.Database;
import com.example.admin.learningenglish.views.ExerciseListenResponseActivity;
import com.example.admin.learningenglish.views.MainActivity;
import com.example.admin.learningenglish.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by admin on 10/7/2017.
 */

public class ListenResponseFragment extends Fragment {
    private ArrayList<String> ten;
    private ListView lvListenResponse;
    private ArrayList<String> dsDiem = new ArrayList<>();
    private ArrayList<String> dsTongDiem = new ArrayList<>();
    private String baiDangHoc = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_listen_response,container,false);

        addControl(view);
        getDatabase();
        khoiTao();
        addEvent();

        return view;
    }

    private void getDatabase() {
        Database database = new Database(getActivity());
        Cursor cursorListenResponse = database.getData("select ten from nghetraloi");
        ten = new ArrayList<>();
        while (cursorListenResponse.moveToNext()) {
            ten.add(cursorListenResponse.getString(0));
        }
    }

    private void khoiTao() {
        getScore();
        ListenResponseAdapter listenResponseAdapter = new ListenResponseAdapter(getActivity(),ten,dsDiem,dsTongDiem);
        lvListenResponse.setAdapter(listenResponseAdapter);
    }

    private void getScore() {
        Database database = new Database(getActivity());

        //lấy điểm
        String sqlDiem = "select diem from nghetraloi";
        Cursor cursorDiem = database.getData(sqlDiem);
        dsDiem = new ArrayList<>();
        while (cursorDiem.moveToNext()) {
            dsDiem.add(cursorDiem.getString(0));
        }

        //lấy tổng điểm
        String sqlTongDiem = "select count(cauhoi.diem) from nghetraloi, bai, cauhoi where nghetraloi.id = bai.idnghetraloi and " +
                "bai.id = cauhoi.idbai group by nghetraloi.id";
        Cursor cursorTongDiem = database.getData(sqlTongDiem);
        dsTongDiem = new ArrayList<>();
        while (cursorTongDiem.moveToNext()) {
            dsTongDiem.add(cursorTongDiem.getString(0));
        }
    }

    private void addEvent() {
        lvListenResponse.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getActivity(), ExerciseListenResponseActivity.class);
                String baiHoc = String.valueOf(i + 1);
                intent.putExtra("baihoc",baiHoc);
                startActivity(intent);
            }
        });

        MainActivity.btnHocTiep.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String baiHoc = getBaiHoc();
                Intent intent = new Intent(getActivity(), ExerciseListenResponseActivity.class);
                intent.putExtra("baihoc",baiHoc);
                startActivity(intent);
            }
        });
    }

    private String getBaiHoc() {
        String baiHoc = "";
        for (int i = 0 ; i < ten.size() ; i++) {
            if (ten.get(i).equals(baiDangHoc)) {
                baiHoc = String.valueOf(i + 1);
                break;
            }
        }

        return baiHoc;
    }

    private void addControl(View view) {
        lvListenResponse = (ListView) view.findViewById(R.id.lvListenResponse);
    }

    @Override
    public void onResume() {
        super.onResume();
        MainActivity.txtTitle.setText("Listen - Response");

        readStatus();

        if (!baiDangHoc.equals("")) {
            MainActivity.rlActionbarBot.setVisibility(View.VISIBLE);
        }
        else {
            MainActivity.rlActionbarBot.setVisibility(View.GONE);
        }

        MainActivity.txtBaiHoc.setText("Đang học : \"" + baiDangHoc + "\"");
        khoiTao();
    }

    private void readStatus() {
        //tạo folder chua file
        File myFile = new File (Environment.getExternalStorageDirectory() + "/LearningEnglish/ListenResponse");
        if (!myFile.exists()){
            myFile.mkdirs();
        }

        try {
            FileInputStream fis = new FileInputStream(myFile.getAbsolutePath() + "/Status");
            ObjectInputStream ois = new ObjectInputStream(fis);
            baiDangHoc = (String) ois.readObject();

            ois.close();
            fis.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
