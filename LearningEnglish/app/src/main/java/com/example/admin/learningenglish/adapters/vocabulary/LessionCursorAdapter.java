package com.example.admin.learningenglish.adapters.vocabulary;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.example.admin.learningenglish.R;
import com.example.admin.learningenglish.databases.vocabulary.LessionContract.LessionEntry;

import static com.example.admin.learningenglish.databases.vocabulary.LessionContract.EASY;
import static com.example.admin.learningenglish.databases.vocabulary.LessionContract.HARD;
import static com.example.admin.learningenglish.databases.vocabulary.LessionContract.MEDIUM;

/**
 * Created by zznam on 11/4/2017.
 */

public class LessionCursorAdapter extends CursorAdapter  {
    public LessionCursorAdapter(Context context, Cursor c) {
        super(context, c, 0);
    }
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_item_lession, parent, false);

        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView txtLessionId = (TextView) view.findViewById(R.id.txtLessionID);
        TextView txtLevel = (TextView) view.findViewById(R.id.txtLevel);
        TextView txtName = (TextView) view.findViewById(R.id.txtName);
        TextView txtDate = (TextView) view.findViewById(R.id.txtDate);
        TextView txtTime = (TextView) view.findViewById(R.id.txtTime);

        int id = cursor.getInt(cursor.getColumnIndexOrThrow(LessionEntry._ID));
        String name = cursor.getString(cursor.getColumnIndexOrThrow(LessionEntry.COLUMN_LESSION_NAME));
        int level = cursor.getInt(cursor.getColumnIndexOrThrow(LessionEntry.COLUMN_LESSION_LEVEL));
//        int score = cursor.getInt(cursor.getColumnIndexOrThrow(LessionEntry.COLUMN_LESSION_SCORE));
        int numberOfWords = cursor.getInt(cursor.getColumnIndexOrThrow(LessionEntry.COLUMN_NUMBER_OF_WORDS));
        String lastTimeAccessed = cursor.getString(cursor.getColumnIndexOrThrow(LessionEntry.COLUMN_LAST_TIME_ACCESSED));

        txtLessionId.setText(id +"");
        txtLevel.setText(getLevelName(level));
        txtName.setText(name);

        if (lastTimeAccessed != null){
            txtDate.setText(formatDate(lastTimeAccessed));
            txtTime.setText(formatTime(lastTimeAccessed));
        }

    }

    public String getLevelName(int i){
        switch (i){
            case EASY: return "Easy";
            case MEDIUM: return "Medium";
            case HARD: return "Hard";
            default: return "Unknown";
        }
    }
    public String formatDate(String dateString){
        DateFormat dateFormat = new SimpleDateFormat("MM/dd");
        Date date = new Date(Long.parseLong(dateString));
        return dateFormat.format(date);
    }

    public String formatTime(String dateString){
        DateFormat dateFormat = new SimpleDateFormat("HH:mm");
        Date date = new Date(Long.parseLong(dateString));
        return dateFormat.format(date);
    }
}
